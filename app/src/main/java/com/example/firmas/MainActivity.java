package com.example.firmas;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView firma;
    Button btnSave,btnFirmar;
    ImageButton btnSearch;
    EditText txtId, txtNombre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firma=findViewById(R.id.img);

        btnFirmar=findViewById(R.id.btnFirma);
        btnSave=findViewById(R.id.btnSave);
        btnSearch=findViewById(R.id.btnSearch);

        btnSearch.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnFirmar.setOnClickListener(this);

        txtId=findViewById(R.id.txtId);
        txtNombre=findViewById(R.id.txtTitulo);

        btnSave.setEnabled(false);
        btnFirmar.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        if(v==btnSearch)
        {
            //Boton Buscar
            DBHelper dbh=new DBHelper(this,"Firmas",null,1);
            SQLiteDatabase db = dbh.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT Nombre, Firma FROM Usuarios Where Id=?",new String[]{txtId.getText().toString()});
            if(c.moveToFirst()){
                //Si existe
                    //Set Nombre
                txtNombre.setText(c.getString(1));
                    //Set Imagen

                    //Botones
                btnFirmar.setEnabled(true);
                btnSave.setEnabled(true);


            }else{
                txtId.setText("");
                txtNombre.setText("");
                firma.setImageBitmap(null);
                Toast.makeText(this,"No existe el campo",Toast.LENGTH_SHORT);
                btnFirmar.setEnabled(false);
                btnSave.setEnabled(false);
            }
        }
        else if(v==btnSave)
        {

        }
        else if (v==btnFirmar){

        }
    }
}
